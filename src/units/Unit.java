package units;

public abstract class Unit{
	private int strength;
	private int trainingCost;
	private int strengthPointsPrice;
	
	public Unit(int strength, int trainingCost, int strengthPointsPrice) {
		this.strength = strength;
		this.trainingCost = trainingCost;
		this.strengthPointsPrice = strengthPointsPrice;
	}

	public int getStrength() {
		return strength;
	}

	public int getTrainingCost() {
		return trainingCost;
	}

	public int getStrengthPointsPrice() {
		return strengthPointsPrice;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public abstract void train();
	public abstract Unit converter() throws Exception;
	public abstract void fromTransform(int strength);
	protected abstract void addStrength(int points);
}