package units;

public class KnightUnit extends Unit {
	static final int INITIAL_STRENGHT = 10;
	static final int TRAINING_COST = 20;
	static final int STRENGHT_POINTS_PRICE = 7;
	
	public KnightUnit() {
		super(INITIAL_STRENGHT, TRAINING_COST, STRENGHT_POINTS_PRICE);
	}

	@Override
	public void train() {
		this.addStrength(STRENGHT_POINTS_PRICE);
	}

	@Override
	protected void addStrength(int points) {
		this.setStrength(this.getStrength() + points);
	}
	
	@Override
	public Unit converter() throws Exception {
		throw new Exception("This unit can�t be trained");
	}

	@Override
	public void fromTransform(int strength) {
		this.setStrength(strength);
	}
}
