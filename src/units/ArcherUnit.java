package units;

public class ArcherUnit extends Unit {
	static final int INITIAL_STRENGHT = 20;
	static final int TRAINING_COST = 30;
	static final int STRENGHT_POINTS_PRICE = 10;
	static final int TRASNFORM_TO_KNIGHT_PRICE = 40;
	
	public ArcherUnit() {
		super(INITIAL_STRENGHT, TRAINING_COST, STRENGHT_POINTS_PRICE);
	}

	@Override
	public void train() {
		this.addStrength(STRENGHT_POINTS_PRICE);
	}

	@Override
	protected void addStrength(int points) {
		this.setStrength(this.getStrength() + points);
	}
	
	@Override
	public Unit converter() {
		KnightUnit knight = new KnightUnit();
		knight.fromTransform(this.getStrength());
		return knight;
	}

	@Override
	public void fromTransform(int strength) {
		this.setStrength(strength);
	}
}
