package units;

public class PikermanUnit extends Unit {
	static final int INITIAL_STRENGHT = 5;
	static final int TRAINING_COST = 10;
	static final int STRENGHT_POINTS_PRICE = 3;
	static final int TRASNFORM_TO_ARCHER_PRICE = 30;
	
	public PikermanUnit() {
		super(INITIAL_STRENGHT, TRAINING_COST, STRENGHT_POINTS_PRICE);
	}

	@Override
	public void train() {
		this.addStrength(STRENGHT_POINTS_PRICE);
	}

	@Override
	protected void addStrength(int points) {
		this.setStrength(this.getStrength() + points);
	}


	@Override
	public Unit converter() {
		ArcherUnit archer = new ArcherUnit();
		archer.fromTransform(this.getStrength());
		return archer;
	}

	@Override
	public void fromTransform(int strength) {
		
	}

}
