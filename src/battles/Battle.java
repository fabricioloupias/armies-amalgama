package battles;

import armies.Army;

public class Battle {
	private Army armyOne;
	private Army armyTwo;
	private Army winner;
	private int GOLD_TO_WINNER = 100;
	private int TOTAL_UNITS_TO_REMOVE_FOR_LOSER = 2;
	private int TOTAL_UNITS_TO_REMOVE_FOR_TIE = 1;
	
	public Battle(Army armyOne, Army armyTwo) {
		super();
		this.armyOne = armyOne;
		this.armyTwo = armyTwo;
	}
	
	public Army getWinner() {
		return winner;
	}

	/**
	 * @return army to win the battle
	 */
	public Army executeBattle() {
		int armyOnePoints = this.armyOne.getTotalStrengthPoints();
		int armyTwoPoints = this.armyTwo.getTotalStrengthPoints();
		BattleHistory battleHistoryToArmyOne = null;
		BattleHistory battleHistoryToArmyTwo = null;
		
		if(armyOnePoints > armyTwoPoints) {
			battleHistoryToArmyOne = new BattleHistory(this.armyTwo, BattleResult.WON);
			battleHistoryToArmyTwo = new BattleHistory(this.armyOne, BattleResult.LOST);
			this.winner = this.armyOne;
			this.armyOne.addGold(GOLD_TO_WINNER);
			this.armyTwo.removeHighestScoringUnits(TOTAL_UNITS_TO_REMOVE_FOR_LOSER);
		}else if(armyOnePoints == armyTwoPoints){
			battleHistoryToArmyOne = new BattleHistory(this.armyTwo, BattleResult.TIED);
			battleHistoryToArmyTwo = new BattleHistory(this.armyOne, BattleResult.TIED);
			
			this.armyTwo.removeHighestScoringUnits(TOTAL_UNITS_TO_REMOVE_FOR_TIE);
		}else {
			battleHistoryToArmyOne = new BattleHistory(this.armyTwo, BattleResult.LOST);
			battleHistoryToArmyTwo = new BattleHistory(this.armyOne, BattleResult.WON);
			this.winner = this.armyTwo;
			this.armyTwo.addGold(GOLD_TO_WINNER);
			this.armyOne.removeHighestScoringUnits(TOTAL_UNITS_TO_REMOVE_FOR_LOSER);
		}
		
		this.armyOne.addBattleToHistory(battleHistoryToArmyOne);
		this.armyTwo.addBattleToHistory(battleHistoryToArmyTwo);
		
		return this.winner;
	}
}
