package battles;

import armies.Army;

public class BattleHistory {
	private Army armyEnemy;
	private BattleResult result;
	
	public BattleHistory(Army armyEnemy, BattleResult result) {
		this.armyEnemy = armyEnemy;
		this.result = result;
	}

	public Army getArmyEnemy() {
		return armyEnemy;
	}

	public BattleResult getResult() {
		return result;
	}
	
}
