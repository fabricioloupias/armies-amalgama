package armies;

public class ByzantinesArmy extends Army {
	private static int TOTAL_INITIAL_PIKERMANS = 5;
	private static int TOTAL_INITIAL_ARCHERS= 8;
	private static int TOTAL_INITIAL_KNIGHTS = 25;
	
	public ByzantinesArmy() {
		super(TOTAL_INITIAL_PIKERMANS, TOTAL_INITIAL_ARCHERS, TOTAL_INITIAL_KNIGHTS);
	}
}
