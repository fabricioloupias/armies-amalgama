package armies;

public class ChineseArmy extends Army {
	private static int TOTAL_INITIAL_PIKERMANS = 2;
	private static int TOTAL_INITIAL_ARCHERS= 25;
	private static int TOTAL_INITIAL_KNIGHTS = 2;
	
	public ChineseArmy() {
		super(TOTAL_INITIAL_PIKERMANS, TOTAL_INITIAL_ARCHERS, TOTAL_INITIAL_KNIGHTS);
	}
	
}
