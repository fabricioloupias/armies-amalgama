package armies;

import java.util.ArrayList;
import java.util.Comparator;

import battles.BattleHistory;
import units.ArcherUnit;
import units.KnightUnit;
import units.PikermanUnit;
import units.Unit;

public abstract class Army {
	private static int INITIAL_GOLD = 1000;
	private int gold;
	private ArrayList<BattleHistory> battleHistory;
	private ArrayList<Unit> units;
	
	public Army(int totalPikermans,
			int totalKnights, int totalArchers) {
		this.gold = INITIAL_GOLD;
		this.initializeArmy(totalPikermans, totalKnights, totalArchers);
		this.battleHistory = new ArrayList<BattleHistory>();
	}
	
	private void initializeArmy(int totalPikermans,
			int totalKnights, int totalArchers) {
		this.units = new ArrayList<Unit>();
		for (int i = 0; i < totalPikermans; i++) {
			this.units.add(new PikermanUnit());
		}
		
		for (int i = 0; i < totalKnights; i++) {
			this.units.add(new KnightUnit());
		}
		
		for (int i = 0; i < totalArchers; i++) {
			this.units.add(new ArcherUnit());
		}
	}
	
	public int getTotalStrengthPoints() {
		int points = 0;
		
		for (int i = 0; i < units.size(); i++) {
			points += units.get(i).getStrength();
		}

		return points;
	}
	
	public void trainUnit(Unit unit) throws Exception {
		int cost = unit.getTrainingCost();
		
		if(cost <= this.gold) {
			unit.train();
			this.gold -= cost;
		}
		
		throw new Exception("You don't have enough gold");
	}
	
	public void addBattleToHistory(BattleHistory battle) {
		this.battleHistory.add(battle);
	}
	
	public void addGold(int gold) {
		this.gold += gold;
	}
	
	/**
	 * Remove highest scoring units.
	 * If the value is greater than the total in the list, the remaining units are removed
	 * @param unitsToRemove total units to remove from army
	 */
	public void removeHighestScoringUnits(int unitsToRemove) {
		this.units.sort(Comparator.comparing(Unit::getStrength).reversed());
		
		if(unitsToRemove > this.units.size()) {
			this.units = new ArrayList<Unit>();
		}
		
		for (int i = 0; i < unitsToRemove; i++) {
			this.units.remove(i);
		}
	}
	
	public void transformUnit(Unit unit) throws Exception {
		if(this.gold < unit.getTrainingCost()) {
			throw new Exception("You don't have enough gold");
		}
		
		this.gold -= unit.getTrainingCost();
		this.units.add(unit.converter());
	}
}
