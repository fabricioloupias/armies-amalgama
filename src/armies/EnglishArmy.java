package armies;

public class EnglishArmy extends Army {
	private static int TOTAL_INITIAL_PIKERMANS = 10;
	private static int TOTAL_INITIAL_ARCHERS= 10;
	private static int TOTAL_INITIAL_KNIGHTS = 15;
	
	public EnglishArmy() {
		super(TOTAL_INITIAL_PIKERMANS, TOTAL_INITIAL_ARCHERS, TOTAL_INITIAL_KNIGHTS);
	}
}
