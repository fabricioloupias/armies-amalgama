package app;

import armies.Army;
import armies.ByzantinesArmy;
import armies.ChineseArmy;
import armies.EnglishArmy;
import battles.Battle;

public class Main {

	public static void main(String[] args) {
		Army armyOne = new ChineseArmy();
		Army armyTwo = new EnglishArmy();
		Army armyThree = new ByzantinesArmy(); 
		
		try {
			Battle battle = new Battle(armyThree, armyTwo);
			battle.executeBattle();
			
			System.out.println(battle.getWinner());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

}